importScripts('https://storage.googleapis.com/workbox-cdn/releases/6.2.0/workbox-sw.js')

workbox.setConfig({ debug: false })

workbox.router.registerRoute('https://gengns.gitlab.io/qrnoads/', 
  workbox.strategies.staleWhileRevalidate())