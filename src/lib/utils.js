/** utils.js */


/**
 * Check if valid URL
 */
export function is_url(str) {
  try { 
    return Boolean(new URL(str))
  } catch { 
    return false
  }
}