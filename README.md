# QRnoAds
QRnoAds is a Progressive Web App (PWA) to Scan QR codes.

## Get started
1. Just open URL or install the PWA: https://gengns.gitlab.io/qrnoads
2. Center the QR code that you want to scan on the screen and hold your phone steady for a couple of seconds.

## Features
1. Free to use
2. No ads
3. No tracking
4. Lightweight
5. Scan QR codes

## Credits
Copyright © PIMO GAMES
